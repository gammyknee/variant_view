# Variant view

Script to convert individual somatic mutations generated from ALL RNAseq variant pipeline to a GViz genome browser pick to enable us to check each variant

To Run: 

```
Rscript plot_variants.R [variants] [BAM]
```

For issues and improvements add an issue (there will be many no doubt...)

---
Jimmy Breen  
(jimmymbreen@gmail.com)  
2019-07-06